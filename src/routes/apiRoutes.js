const router = require('express').Router();
const userController = require('../controller/userController');
const salaDeAulaController = require('../controller/salaDeAulaController');
const dbController = require('../controller/dbController');

// USUÁRIO
router.post('/loginUsuario/', userController.postLogin); // Rota para fazer login
router.post('/createUsuario/', userController.createUsuario); // Rota para criar usuário
router.put('/updateUsuario/', userController.updateUser);
router.delete('/deleteUsuario/:id_aluno', userController.deleteUser);
router.get('/getUsuario/', userController.getAllUsuarios); // Rota para obter todos os usuários
router.get('/getUsuario/:id_aluno', userController.getUsuarioById); // Rota para obter um usuário pelo ID


// SALA DE AULA
router.post('/createSalaDeAula', salaDeAulaController.createSalaDeAula);
router.get('/getAllSchedules', salaDeAulaController.getAllSalasDeAula);
router.get('/getSalaDeAulaById/:number', salaDeAulaController.getSalaDeAulaById);
router.put('/updateSalaDeAula/:number', salaDeAulaController.updateSalaDeAula);
router.delete('/deleteSalaDeAula/:number', salaDeAulaController.deleteSalaDeAula);

// API COM BANCO DE DADOS, ROTA PARA CONSULTAR AS TABELAS DO BANCO
router.get('/tables/', dbController.getNameTables);
router.get('/tablesdescriptions/', dbController.getTablesDescription); 

module.exports = router;
